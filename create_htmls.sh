jupyter nbconvert 0_load_data.ipynb --to html --output htmls/0_load_data.html
jupyter nbconvert 1_tskmeans_30_days.ipynb --to html --output htmls/1_tskmeans_30_days.html
jupyter nbconvert 2_tskmeans_72_hours.ipynb --to html --output htmls/2_tskmeans_72_hours.html
jupyter nbconvert 3_tskmeans_60_minutes.ipynb --to html --output htmls/3_tskmeans_60_minutes.html