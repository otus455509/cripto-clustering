import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tslearn.clustering import TimeSeriesKMeans, silhouette_score
from tqdm import tqdm


def visualize_ts(data, ticker, title):
    tmp = data[data['name'] == ticker].copy()

    fig = plt.figure(figsize=(15,4))
    z = sns.scatterplot(data=tmp, x='time', y='open', label='Open Price', color='black')
    a = sns.lineplot(data=tmp, x='time', y='high', label='High', color='r')
    b = sns.lineplot(data=tmp, x='time', y='low', label='Low', color='b')
    c = sns.lineplot(data=tmp, x='time', y='mean', label='Mean', color='g')

    line = c.get_lines()
    plt.fill_between(line[0].get_xdata(), line[0].get_ydata(), line[1].get_ydata(), color='orange', alpha=.5)

    plt.title(title)
    plt.xlabel("Time")
    plt.ylabel("Price")
    for num, label in enumerate(c.get_xticklabels()):
        if num % 3 == 0:  
            label.set_visible(True)
        else:
            label.set_visible(False)
    plt.xticks(rotation=45, ha='right')
    plt.grid(color='0.85')
    plt.legend()
    plt.show()
    plt.close()


def plot_cluster_tickers(current_cluster, max_tickers=16):
    fig, ax = plt.subplots(
        int(np.ceil(min(current_cluster.shape[0], max_tickers) / 4)),
        4,
        figsize=(16, int(np.ceil(min(current_cluster.shape[0], max_tickers))))
    )
    fig.autofmt_xdate(rotation=45)
    ax = ax.reshape(-1)

    for index, (_, row) in enumerate(current_cluster.iterrows()):
        if index >= max_tickers: break
        ax[index].plot(row.iloc[1:-1])
        ax[index].set_title(row['name'])
        for n, label in enumerate(ax[index].xaxis.get_ticklabels()):
            if n % 6 != 0:
                label.set_visible(False)
        ax[index].grid(color='0.85')
        plt.xticks(rotation=45)

    plt.tight_layout()
    plt.show()


def create_ts_frames(data, scaler):
    data.sort_values(by='time', inplace=True)
    df_ts = data.pivot(columns='time_str', index='name', values='mean')
    df_ts_scaled = scaler.fit_transform(df_ts.to_numpy().T).T
    # Уберём из рассмотрения монеты с близким к 0 стандартному отклонению
    df_ts = df_ts.loc[~np.isclose(df_ts_scaled.std(axis=1), 0)]
    df_ts_scaled = df_ts_scaled[~np.isclose(df_ts_scaled.std(axis=1), 0)]
    return df_ts, df_ts_scaled


def plot_cluster_num_metrics(data, metric="euclidean"):
    distortions = []
    silhouette = []
    K = range(2, 10)
    for k in tqdm(K):
        kmeanModel = TimeSeriesKMeans(n_clusters=k, metric=metric, n_jobs=6, max_iter=10, n_init=5)
        kmeanModel.fit(data)
        distortions.append(kmeanModel.inertia_)
        silhouette.append(silhouette_score(data, kmeanModel.labels_, metric=metric))
        
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    ax1.plot(K, distortions, 'b-', marker='*')
    ax2.plot(K, silhouette, 'r-', marker='o')

    ax1.set_xlabel("Число кластеров")
    ax1.set_ylabel('Distortion', color='b')
    ax2.set_ylabel('Silhouette', color='r')
    ax1.grid(color='0.85')

    plt.show()